function loadFile(filePath) {
  var result = null;
  var request = new XMLHttpRequest();
  request.open("GET", filePath, false);
  try {
    request.send();
  }
  catch {
    //check cdn for extras as a last resort
    if (filePath.startsWith(`https://`)) filePath = filePath.slice(8);
    if (filePath.startsWith(`http://`)) filePath = filePath.slice(7);
    request.open("GET", `https://cdn.ambrosia.moe/extras/${filePath}`, false);
    request.send();
  }
  if (request.status == 200) {
    result = request.responseText;
    console.log(request.getResponseHeader("Content-Type"));
  }
  return result;
}


var filesystem = {
  "avatar.png": { name: "avatar.png", type: "file" },
  "blobcat.gif": { name: "blobcat.gif", type: "file" },
  "friends.txt": { name: "friends.txt", type: "file" },
  "test": {
    name: "test", type: "folder", files: {
      "test.txt": { name: "test.txt", type: "file" }
    }
  }

};
var fsfolder = filesystem;
var fsposition = [];
function getFolder() {
  fsfolder = filesystem;
  fsposition.forEach((folder) => {
    fsfolder = fsfolder[folder].files;
    console.log(fsfolder);
  });
}
jQuery(function ($, undefined) {
  var term = $('body').terminal(async function (command) {
    const { name, args, rest } = $.terminal.parse_command(command);
    const options = $.terminal.parse_options(args);
    console.log(options);
    if (name === 'cat') {
      var dir = '';
      fsposition.forEach((folder) => {
        dir += `${folder}/`;
      });
      args.forEach((filename) => {
        if (filename.endsWith('.png') || filename.endsWith('.jpg') || filename.endsWith('.gif') || filename.endsWith('.webp')) {
          this.echo($('<img src="files/' + dir + filename + '" style="height:300px;padding:5px;">'));
        } else {
          var file = loadFile(`files/${dir}${filename}`);
          if (file == null) {
            this.echo(`cat: ${filename}: no such file or directory`);
          } else {
            this.echo($(`<pre>${file}</pre>`));
          }
        }
      });
    } else if (name === 'curl') {
      if (!link.startsWith(`https://`) && !link.startsWith(`http://`)) {
        link = `https://${link}`;
      }
      if (link == "https://ifconfig.me") link += '/ip'

      if (link.endsWith('.png') || link.endsWith('.jpg') || link.endsWith('.gif') || link.endsWith('.webp')) {
        this.echo($(`<img src="${link}" style="height:300px;padding:5px;">`));
      } else {
        var file = loadFile(link);
        if (file == null) {
          this.echo(`curl: ${link}: could not resolve file`);
        } else {
          this.echo($(`<pre>${file}</pre>`));
        }
      }
    } else if (name === 'cls') {
      $('.terminal-output').html('');
    } else if (name === 'echo') {
      this.echo(args, rest);
    } else if (name === 'help') {
      //TODO: add help for each command
      this.echo($('<pre>cat cd cls curl <br>help ls neofetch</pre>'));
    } else if (name === 'less') {
      $.get(`files/${file}`, (text) => this.less(text));
    } else if (name === 'ls') {
      for (const [_, item] of Object.entries(fsfolder)) {
        if (item.type == "folder") {
          this.echo($('<pre><b class="cyan">' + item.name + '</b>/</pre>'));
        } else {
          this.echo(item.name);
        }
      }
    } else if (name === 'cd') {
      var oldfsposition = [...fsposition];
      args[0].split('/').every((folder) => {
        if (folder == '..') {
          if (fsposition.length != 0) {
            fsposition.pop();
          }
        } else {
          if (folder in fsfolder) {
            if (fsfolder[folder].type == 'folder') {
              fsposition.push(folder);
            } else {
              this.echo(`cd: '${dir}' is not a directory`);
              fsposition = oldfsposition;
              getFolder();
              return false;
            }
          }
          else {
            this.echo(`cd: The directory '${dir}' does not exist`);
            fsposition = oldfsposition;
            getFolder();
            return false;
          }
        }
        getFolder();
        return true
      });
      var cdir = '~';
      fsposition.forEach((folder) => {
        cdir += '/' + folder;
      });
      this.set_prompt(`moshi ${cdir}>`);
    } else if (name === 'neofetch') {
      this.echo($($('#neofetch').html()));
    }

  }, {
    completion: ['cd', 'cat', 'cls', 'curl', 'help', 'ls', 'neofetch'],
    //height: 850,
    //width: 1700,
    prompt: 'moshi ~>',
    greetings: '',
    pipe: true
  });
  $.terminal.new_formatter([/\[([^]+)\]\((.*?)\)/g, function (_, label, url) {
    return `[[!;;;;${url}]${label}]`;
  }]);
  term.exec('neofetch');
});
